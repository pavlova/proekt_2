<!-- NAVBAR -->
<div class="container-fluid navbar navbar-colorzz">
    <div class="collapse navbar-collapse collapse-n" id="bs-example-navbar-collapse-1">
        <div class="col-md-2 col-xs-4">
            <div id="main">
                <span class="hambur-m" id='navbarCollapseDown'>&#9776;
                    <i class="fas fas-b fa-chevron-up"></i></span>
                <span class="hambur-m-Mobile" data-toggle="modal" data-target="#myModalMOBILEv">&#9776;
                    <i class="fas fas-b fa-chevron-up"></i></span>
            </div>
        </div>
        <div class="col-md-8 hidden-xs hidden-sm text-right">
            <div id="wrap">
                <form action="" autocomplete="off">
                    <input class="input-search-n" id="search" name="search" type="text"
                        placeholder="Search Articles:"><input id="search_submit" value="Rechercher" type="search">
                </form>
            </div>
        </div>
        <div class="col-md-2 col-xs-8 text-right">
            @if (Session::has('firstname') && Session::has('lastname'))
            <button type="button" class="btn btn-modal1-e dropdown-toggle" data-toggle="dropdown"><i
                    class="fas fa-user"></i> {{ session('firstname') }}
                {{ session('lastname') }}</button>
            <ul class="dropdown-menu">
                <li><a href="{{ route('myprofile') }}" role="button" class="btnLogOut-e">My Profile</a></li>
                <li><a href="{{ route('logout') }}" role="button" class="btnLogOut-e">Log Out</a></li>
            </ul>
            @else
            <button type="button" class="btn btn-modal1-e" data-toggle="modal" data-target="#myModal"><i
                    class="fas fa-user"></i>
                Sign In / Register</button>
            @endif
        </div>
    </div>
</div>
<div class="collapse collapse-b" id="collapseExample" style="position:absolute;">
    <div class="col-md-12">
        <ul class="nav nav-pills nav-b">
            <li><a href="#">ABOUT PET ADOPTION</a></li>
            <li><a href="#">DOG ADAPTION</a></li>
            <li><a href="#">CAT ADAPTION</a></li>
            <li><a href="#">PET ADAPTION STORIES</a></li>
            <li><a href="#">INFORMATION</a></li>
            <li><a href="#">VIDEOS</a></li>
        </ul>
    </div>
</div>
 
<div class="modal fade" id="myModalMOBILEv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
            <div class="modal-header modal-header-mobile">
                    <button type="button" class="close close-mobile" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
                  </div>
        
        <div class="modal-body-Mobile">
                <div class="col-sm-12">
                        <ul class="nav nav-b-mobile">
                            <li><a href="#">ABOUT PET ADOPTION</a></li>
                            <li><a href="#">DOG ADAPTION</a></li>
                            <li><a href="#">CAT ADAPTION</a></li>
                            <li><a href="#">PET ADAPTION STORIES</a></li>
                            <li><a href="#">INFORMATION</a></li>
                            <li><a href="#">VIDEOS</a></li>
                        </ul>
                </div>
                <div class="col-sm-12 search-back">
                        <div class="input-group input-group-mobile">
                                <input type="text" class="form-control form-contorol-Search" placeholder="Search for...">
                                <span class="input-group-btn">
                                  <button class="btn btn-default btn-mobile" type="button"><span class="glyphicon glyphicon-search glyphicon-search-mobile" aria-hidden="true"></span></button>
                                </span>
                        </div><!-- /input-group -->
                </div>
            </div>
        
      </div>
    </div>
  </div>