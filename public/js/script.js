/* HOME PAGE */

var x = document.getElementById("navbarCollapseDown");
x.addEventListener('click', function (event) {
    event.preventDefault();
    event.stopPropagation();
    var y = document.getElementById('collapseExample');
    if (y.classList.contains('in')) {
        // The box that we clicked has a class of bad so let's remove it and add the good class
        y.classList.remove('in');
    } else {
        // The user obviously can't follow instructions so let's alert them of what is supposed to happen next
        y.classList.add('in');
    }
});

$(document).on('click', '.flip-card', function (event) {
    // event.preventDefault();
    event.stopPropagation();
    $('.flip-card').removeClass('active');
    $(this).addClass('active');
});

/* HOME PAGE - FINISH */

/* PET PROFILE */

// Get the modal
var modal = document.getElementById('myModalPet');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('petImgModal');
var modalImg = document.getElementById("imgPet");
if (img) {
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
    }
}


// Get the <span> element that closes the modal
var span = document.getElementsByClassName("closeBtn-e")[0];

// When the user clicks on <span> (x), close the modal
if (span) {
    span.onclick = function () {
        modal.style.display = "none";
    }
}


/* PET PROFILE - FINISH */

function fbs_click() {
    u = location.href;
    t = document.title;
    window.open('http://www.facebook.com/sharer.php?u=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t), 'sharer', 'toolbar=0,status=0,width=626,height=436');
    return false;
}

var copyLink = document.getElementsByClassName('copyToClipboard');

for (let i = 0; i < copyLink.length; i++) {
    const element = copyLink[i];
    element.addEventListener('click', copyToClipboardF);    
}

function copyToClipboardF() {
    var copyText = document.getElementById("textToCopy");
    copyText.value = this.dataset.target;
    copyText.select();
    document.execCommand("copy");
}
