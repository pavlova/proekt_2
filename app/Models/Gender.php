<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    public function pet_profiles()
    {
        return $this->hasMany('App\Pet_Profile');
    }
}
