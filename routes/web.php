<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/')->name('homepage')->uses('PetFinderController@showHomePage');

Route::get('/home')->name('homepage')->uses('PetFinderController@showHomePage');

Route::get('/home/myprofile')->name('myprofile')->uses('PetFinderController@showMyProfile');

Route::get('/home/petprofile/{id}')->name('petprofile')->uses('PetFinderController@showPetProfile');

Route::get('/logout')->name('logout')->uses('LoginController@logout');

Route::get('/dashboard')->name('dashboard')->uses('SearchController@showDashboard');

Route::post('/register/post')->name('register.post')->uses('RegisterController@sendRegisterPost');

Route::post('/register/myprofile/post')->name('myprofile.post')->uses('RegisterController@sendMyProfilePost');

Route::post('/login/post')->name('login.post')->uses('LoginController@sendLoginPost');

Route::post('/dashboard/search')->name('search.post')->uses('SearchController@searchPost');