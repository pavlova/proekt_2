<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountriesTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(GendersTableSeeder::class);
       	$this->call(TypesTableSeeder::class);
        $this->call(SizeTableSeeder::class);
        $this->call(Coat_LengthsTableSeeder::class);
        $this->call(User_profilesTableSeeder::class);
        $this->call(Pet_profilesTableSeeder::class);
    }
}
