<!-- FOOTER -->
<div class="container-fluid hidden-sm footer-e">
    <div class="col-md-2 col-md-offset-1 col-xs-6 color-e">
        <a href="#" class="link1-e">ABOUT US</a>
        <a href="#">FAQs</a>
        <a href="#">Partnerships</a>
        <a href="#">Terms of Service</a>
        <a href="#">Mobile Site & Apps</a>
        <a href="#">Foundation</a>
        <a href="#">Free Widgets & Graphics</a>
        <a href="#">Press</a>
        <a href="#">For Developers</a>
        <a href="#">Contact Us</a>
    </div>
    <div class="col-md-2 col-xs-6 color-e">
        <a href="#" class="link1-e">PET ADOPTION</a>
        <a href="#">Dog Adoption</a>
        <a href="#">Cat Adoption</a>
        <a href="#">Other Pet Adoption</a>
        <a href="#">Search Adoption Organizations</a>
        <a href="#">Pet-Adoption Stories</a>
        <a href="#">Local Adoption Events</a>
        <a href="#">Shelters & Rescues</a>
    </div>
    <div class="col-md-2 col-xs-6 color-e p-top-e">
        <a href="#" class="link1-e">PET CARE TOPICS</a>
        <a href="#">Dog Care</a>
        <a href="#">Dog Breeds</a>
        <a href="#">Cat Care</a>
        <a href="#">Cat Breeds</a>
        <a href="#">All Pet Care</a>
        <a href="#">Pet Care Videos</a>
        <a href="#">Helping Pets</a>
    </div>
    <div class="col-md-2 col-xs-6 white-e p-top-e">
        <a href="#">SITEMAP</a>
        <a href="#">PRIVACY POLICY</a>
        <a href="#">ABOUT OUR ADS</a>
        <a href="#">SHELTER & RESCUE LOGIN</a>
        <a href="#">SHELTER & RESCUE REGISTRATION</a>
    </div>
    <div class="col-md-2 col-xs-12 p-top-e">
        <p class="text-e">To get the latest on pet adoption and pet care, sign up for the newsletter.</p>
        <form method="" action="">
            <div class="form-group">
                <div class="input-group">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                    <span class="input-group-addon back-color-e"><i class="fas fa-angle-right"></i></span>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="container-fluid hidden-lg hidden-md hidden-xs footer-e">
    <div class="container flex-e">
        <div class="col-sm-3 color-e">
            <a href="#" class="link1-e">ABOUT US</a>
            <a href="#">FAQs</a>
            <a href="#">Partnerships</a>
            <a href="#">Terms of Service</a>
            <a href="#">Mobile Site & Apps</a>
            <a href="#">Foundation</a>
            <a href="#">Free Widgets & Graphics</a>
            <a href="#">Press</a>
            <a href="#">For Developers</a>
            <a href="#">Contact Us</a>
        </div>
        <div class="col-sm-3 color-e">
            <a href="#" class="link1-e">PET ADOPTION</a>
            <a href="#">Dog Adoption</a>
            <a href="#">Cat Adoption</a>
            <a href="#">Other Pet Adoption</a>
            <a href="#">Search Adoption Organizations</a>
            <a href="#">Pet-Adoption Stories</a>
            <a href="#">Local Adoption Events</a>
            <a href="#">Shelters & Rescues</a>
        </div>
        <div class="col-sm-3 color-e">
            <a href="#" class="link1-e">PET CARE TOPICS</a>
            <a href="#">Dog Care</a>
            <a href="#">Dog Breeds</a>
            <a href="#">Cat Care</a>
            <a href="#">Cat Breeds</a>
            <a href="#">All Pet Care</a>
            <a href="#">Pet Care Videos</a>
            <a href="#">Helping Pets</a>
        </div>
    </div>
    <div class="container flex-e top-e">
        <div class="col-sm-4 white-e">
            <a href="#">SITEMAP</a>
            <a href="#">PRIVACY POLICY</a>
            <a href="#">ABOUT OUR ADS</a>
            <a href="#">SHELTER & RESCUE LOGIN</a>
            <a href="#">SHELTER & RESCUE REGISTRATION</a>
        </div>
        <div class="col-sm-4">
            <p class="text-e">To get the latest on pet adoption and pet care, sign up for the newsletter.</p>
            <form method="" action="">
                <div class="form-group">
                    <div class="input-group">
                        <input type="email" class="form-control" id="emailF" name="emailF" placeholder="Email">
                        <span class="input-group-addon back-color-e"><i class="fas fa-angle-right"></i></span>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container-fluid social-e">
    <div class="col-md-7 col-md-offset-1 col-sm-6 col-sm-offset-1 col-xs-2">
        <p>&copy;2019</p>
    </div>
    <div class="col-md-3 col-sm-4 col-xs-10 icons-e text-right">
        <a href="https://www.facebook.com/Petfinder"><i class="fab fa-facebook-f"></i></a>
        <a href="https://twitter.com/Petfinder"><i class="fab fa-twitter"></i></a>
        <a href="https://www.instagram.com/petfinder/"><i class="fab fa-instagram"></i></a>
        <a href="https://www.youtube.com/channel/UC85FmJhwKDfiXfialMvyM5g"><i class="fab fa-youtube"></i></a>
        <a href="https://www.pinterest.com/petfinder/"><i class="fab fa-pinterest-p"></i></a>
    </div>
</div>
<div class="copyHidden">
    <input type="text" id="textToCopy">
</div>