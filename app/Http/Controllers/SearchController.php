<?php

namespace App\Http\Controllers;

use App\Models\Size;
use App\Models\Gender;
use App\Models\Country;
use App\Models\Coat_Length;
use App\Models\Pet_Profile;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function showDashboard() 
    {
        $page_title = 'Dashboard';
        $countries = Country::all();
        $sizes = Size::all();
        $coat_lenghts = Coat_Length::all();
        $genders = Gender::all();

        $petSearch = Pet_Profile::all();
        $petInfo = PetFinderController::getPetData($petSearch);            
        
        return view('dashboard', compact('page_title', 'countries', 'sizes', 'coat_lenghts', 'genders', 'petInfo'));
    }

    public function searchPost(Request $request)
    {
        $page_title = 'Dashboard';
        $countries = Country::all();
        $sizes = Size::all();
        $coat_lenghts = Coat_Length::all();
        $genders = Gender::all();

        if ($request->sizeSelect == '') {
            if ($request->coatLenghtSelect == '') {
                if ($request->genderSelect == '') {
                    
                    $petSearch = Pet_Profile::all();
                    $petInfo = PetFinderController::getPetData($petSearch);

                } else {

                    $petSearch = Pet_Profile::where ('gender_id', $request->genderSelect)->get();
                    $petInfo = PetFinderController::getPetData($petSearch);

                }
            }  else {
                if ($request->genderSelect == '') {
                   
                    $petSearch = Pet_Profile::where ('coat_length_id', $request->coatLenghtSelect)->get();
                    $petInfo = PetFinderController::getPetData($petSearch);

                } else {

                    $petSearch = Pet_Profile::where (['coat_length_id' => $request->coatLenghtSelect, 'gender_id' => $request->genderSelect] )->get();
                    $petInfo = PetFinderController::getPetData($petSearch);

                }                   
            }
        } else {    
            if ($request->coatLenghtSelect == '') {
                if ($request->genderSelect == '') {

                    $petSearch = Pet_Profile::where ('size_id', $request->sizeSelect)->get();
                    $petInfo = PetFinderController::getPetData($petSearch);

                } else {
            
                    $petSearch = Pet_Profile::where (['size_id' => $request->sizeSelect, 'gender_id' => $request->genderSelect] )->get();
                    $petInfo = PetFinderController::getPetData($petSearch);

                }
            }  else {
                if ($request->genderSelect == '') {

                    $petSearch = Pet_Profile::where (['size_id' => $request->sizeSelect, 'coat_length_id' => $request->coatLenghtSelect] )->get();
                    $petInfo = PetFinderController::getPetData($petSearch);

                } else {

                    $petSearch = Pet_Profile::where (['size_id' => $request->sizeSelect, 'coat_length_id' => $request->coatLenghtSelect, 'gender_id' => $request->genderSelect] )->get();
                    $petInfo = PetFinderController::getPetData($petSearch);

                }                   
            }    
        }

          return view('dashboard', compact('page_title', 'countries', 'sizes', 'coat_lenghts', 'genders', 'petInfo'));   
    }
}