@extends ('layouts/master')

@section('myprofile')
<div class="container-fluid backTabPanel-e">
    <div class="container tabpanel-e">
        <h2 class="myProfile-e">My Profile</h2>
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#about" aria-controls="about" role="tab"
                    data-toggle="tab">About Me</a></li>
            <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Account
                    Settings</a>
            </li>
            <li role="presentation"><a href="#saved" aria-controls="saved" role="tab" data-toggle="tab">Saved
                    Searches & Alerts</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="about">
                <form method="POST" enctype="multipart/form-data" action="{{ route('myprofile.post') }}">
                    @csrf
                    <div class="container-fluid p0-e">
                        <div class="col-md-12">
                            <h5 class="first-e">WHAT IS YOUR NAME?</h5>
                        </div>
                        <div class="col-md-6">
                            @foreach($errors->get('firstMyProfile') as $error)
                            <p class="error">{{ $error }}</p>
                            @endforeach
                            <div class="form-group form-group-e">
                                <span>First name</span>
                                <input type="text" class="form-control input-e" name="firstMyProfile"
                                    value="{{ session('firstname') }}" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-6">
                            @foreach($errors->get('lastMyProfile') as $error)
                            <p class="error">{{ $error }}</p>
                            @endforeach
                            <div class="form-group form-group-e">
                                <span>Last name</span>
                                <input type="text" class="form-control input-e" name="lastMyProfile"
                                    value="{{ session('lastname') }}" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h5 class="second-e">HOW CAN YOU BE REACHED?</h5>
                        </div>
                        <div class="col-md-6">
                            @foreach($errors->get('phoneMyProfile') as $error)
                            <p class="error">{{ $error }}</p>
                            @endforeach
                            <div class="form-group form-group-e">
                                <span>Phone number</span>
                                <input type="text" class="form-control input-e" name="phoneMyProfile"
                                    value="{{ old('phoneMyProfile') }}" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h5 class="second-e">WHERE DO YOU LIVE?</h5>
                        </div>
                        <div class="col-md-12 p0-e">
                            <div class="col-md-6">
                                @foreach($errors->get('countryMyProfile') as $error)
                                <p class="error">{{ $error }}</p>
                                @endforeach
                                <div class="form-group form-group-e inputBottom-e">
                                    <span>Country (required)</span>
                                    <select class="form-control input-e" name="countryMyProfile">
                                        <option value="">Choose country</option>
                                        @foreach ($countries as $country)
                                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            @foreach($errors->get('streetMyProfile') as $error)
                            <p class="error">{{ $error }}</p>
                            @endforeach
                            <div class="form-group form-group-e inputBottom-e">
                                <span>Street Address</span>
                                <input type="text" class="form-control input-e" name="streetMyProfile"
                                    value="{{ old('streetMyProfile') }}" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-6">
                            @foreach($errors->get('streetMyProfileC') as $error)
                            <p class="error">{{ $error }}</p>
                            @endforeach
                            <div class="form-group form-group-e inputBottom-e">
                                <span>Street Address (continued)</span>
                                <input type="text" class="form-control input-e" name="streetMyProfileC"
                                    value="{{ old('streetMyProfileC') }}" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-6">
                            @foreach($errors->get('cityMyProfile') as $error)
                            <p class="error">{{ $error }}</p>
                            @endforeach
                            <div class="form-group form-group-e inputBottom-e">
                                <span>City / Town</span>
                                <input type="text" class="form-control input-e" name="cityMyProfile"
                                    value="{{ old('cityMyProfile') }}" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-6">
                            @foreach($errors->get('stateMyProfile') as $error)
                            <p class="error">{{ $error }}</p>
                            @endforeach
                            <div class="form-group form-group-e inputBottom-e">
                                <span>State</span>
                                <select class="form-control input-e" name="stateMyProfile">
                                    <option value="">Choose a state</option>
                                    @foreach ($states as $state)
                                    <option value="{{ $state->id }}">{{ $state->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 p0-e">
                            <div class="col-md-6">
                                @foreach($errors->get('zipCodeMyProfile') as $error)
                                <p class="error">{{ $error }}</p>
                                @endforeach
                                <div class="form-group form-group-e inputBottom-e">
                                    <span>ZIP code</span>
                                    <input type="text" class="form-control input-e placeHolder-e"
                                        name="zipCodeMyProfile" value="{{ old('zipCodeMyProfile') }}" autocomplete="off"
                                        placeholder="90210">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 p0-e">
                            <div class="col-md-6">
                                <h5 class="third-e">YOUR CURRENT PETS</h5>
                                <button type="button" class="btn btn-addPet-e btn-block">ADD A PET</button>
                            </div>
                        </div>
                        <div class="container-fluid p0-e">
                            <div class="col-md-12 topAddPet-e p0-e">
                                <div class="col-md-4">
                                    @foreach($errors->get('petName') as $error)
                                    <p class="error">{{ $error }}</p>
                                    @endforeach
                                    <div class="form-group">
                                        <span class="petNameInput-e">Pet Name</span>
                                        <input type="text" class="form-control inputPet-e" name="petName"
                                            value="{{ old('petName') }}" autocomplete="off" placeholder="Name">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    @foreach($errors->get('petType') as $error)
                                    <p class="error">{{ $error }}</p>
                                    @endforeach
                                    <div class="form-group">
                                        <span class="petNameInput-e">Type</span>
                                        <select class="form-control inputPet-e" name="petType">
                                            <option value="">Choose...</option>
                                            @foreach ($types as $type)
                                            <option value="{{ $type->id }}">{{ $type->type }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 p0-e">
                                <div class="col-md-8">
                                    @foreach($errors->get('petLocation') as $error)
                                    <p class="error">{{ $error }}</p>
                                    @endforeach
                                    <div class="form-group">
                                        <span class="petNameInput-e">Location</span>
                                        <input type="text" class="form-control inputPet-e" name="petLocation"
                                            value="{{ old('petLocation') }}" autocomplete="off" placeholder="Location">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 p0-e">
                                <div class="col-md-8">
                                    @foreach($errors->get('petImage') as $error)
                                    <p class="error">{{ $error }}</p>
                                    @endforeach
                                    <div class="form-group">
                                        <span class="petNameInput-e">Image</span>
                                        <input type="file" name="petImage" class="file file-img">
                                        <div class="input-group col-xs-12">
                                        <span class="input-group-addon inputPet-e-img"><i class="glyphicon glyphicon-picture"></i></span>
                                        <input type="text" class="form-control input-lg " disabled placeholder="Upload Image">
                                        <span class="input-group-btn">
                                        <button class="browse btn btn-img input-lg" type="button"><i class="glyphicon glyphicon-search"></i> Browse</button>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 p0-e">
                                <div class="col-md-8">
                                    @foreach($errors->get('petShortDesc') as $error)
                                    <p class="error">{{ $error }}</p>
                                    @endforeach
                                    <div class="form-group">
                                        <span class="petNameInput-e">Short Description</span>
                                        <input type="text" class="form-control inputPet-e" name="petShortDesc" value="{{ old('petShortDesc') }}"
                                            autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 p0-e">
                                <div class="col-md-4">
                                    @foreach($errors->get('petCharacteristics') as $error)
                                    <p class="error">{{ $error }}</p>
                                    @endforeach
                                    <div class="form-group">
                                        <span class="petNameInput-e">Characteristics</span>
                                        <input type="text" class="form-control inputPet-e" name="petCharacteristics"
                                            value="{{ old('petCharacteristics') }}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    @foreach($errors->get('petCoatLength') as $error)
                                    <p class="error">{{ $error }}</p>
                                    @endforeach
                                    <div class="form-group">
                                        <span class="petNameInput-e">Coat Length</span>
                                        <select class="form-control inputPet-e" name="petCoatLength">
                                            <option value="">Choose...</option>
                                            @foreach ($coat_lengths as $coat_length)
                                            <option value="{{ $coat_length->id }}">{{ $coat_length->coat_length }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 p0-e">
                                <div class="col-md-3">
                                    @foreach($errors->get('petHouseTrained') as $error)
                                    <p class="error">{{ $error }}</p>
                                    @endforeach
                                    <div class="form-group">
                                        <span class="petNameInput-e">House trained</span>
                                        <select class="form-control inputPet-e" name="petHouseTrained">
                                            <option value="">Choose...</option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    @foreach($errors->get('petGoodInHome') as $error)
                                    <p class="error">{{ $error }}</p>
                                    @endforeach
                                    <div class="form-group">
                                        <span class="petNameInput-e">Good in a home with</span>
                                        <input type="text" class="form-control inputPet-e" name="petGoodInHome" value="{{ old('petGoodInHome') }}"
                                            autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 p0-e">

                                <div class="col-md-4">
                                    @foreach($errors->get('petSize') as $error)
                                    <p class="error">{{ $error }}</p>
                                    @endforeach
                                    <div class="form-group">
                                        <span class="petNameInput-e">Size</span>
                                        <select class="form-control inputPet-e" name="petSize">
                                            <option value="">Choose...</option>
                                            @foreach ($sizes as $size)
                                            <option value="{{ $size->id }}">{{ $size->pet_size }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        @foreach($errors->get('petGender') as $error)
                                        <p class="error">{{ $error }}</p>
                                        @endforeach
                                        <span class="petNameInput-e">Gender</span>
                                        <select class="form-control inputPet-e" name="petGender">
                                            <option value="">Choose...</option>
                                            @foreach ($genders as $gender)
                                            <option value="{{ $gender->id }}">{{ $gender->gender }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 p0-e">
                                <div class="col-md-8">
                                    @foreach($errors->get('petLongDesc') as $error)
                                    <p class="error">{{ $error }}</p>
                                    @endforeach
                                    <div class="form-group">
                                        <span class="petNameInput-e">Long description</span>
                                        <textarea class="form-control inputPet-e" name="petLongDesc"
                                            rows="3">{{ old('petLongDesc') }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 p0-e">
                                <div class="col-md-8">
                                    <button type="submit" class="btn btn-addPetOne-e">Add a Pet</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <h5 class="serachBottom-e">You may <a href="#" class="searchPet-e">search our
                                    database</a>
                                of thousands of pets looking
                                for forever homes.</h5>
                        </div>
                    </div>
                </form>
            </div>
            <div role="tabpanel" class="tab-pane tabHeight-e" id="settings">
                <div class="container-fluid">
                    <!--<form method="" action=""> !-->
                    <div class="col-md-12">
                        <h5 class="first-e">EMAIL</h5>
                    </div>
                    <div class="col-md-4">
                        <input type="email" class="form-control inputOldValue-e" name="emailOld"
                            value="brainster@brainster.co" autocomplete="off">
                    </div>
                    <div class="col-md-2 btnChange-e p0-e">
                        <button type="button" class="btn btn-change-e">Change</button>
                    </div>
                    <div class="col-md-12">
                        <h5 class="second-e">CHANGE PASSWORD</h5>
                    </div>
                    <div class="col-md-12 p0-e">
                        <div class="col-md-6">
                            <div class="form-group form-group-e inputBottom-e">
                                <span>Current password</span>
                                <input type="text" class="form-control input-e placeHolder-e"
                                    name="currentPassMyProfile" value="" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 p0-e">
                        <div class="col-md-6">
                            <div class="form-group form-group-e inputBottom-e">
                                <span>New password</span>
                                <input type="text" class="form-control input-e placeHolder-e" name="newPassMyProfile"
                                    value="" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 p0-e">
                        <div class="col-md-6">
                            <div class="form-group form-group-e inputBottom-e">
                                <span>Confirm password</span>
                                <input type="text" class="form-control input-e placeHolder-e"
                                    name="confirmPassMyProfile" value="" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 p0-e">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-addPet-e btn-block">UPDATE PASSWORD</button>
                        </div>
                    </div>
                    <!-- </form> !-->
                </div>
            </div>
            <div role="tabpanel" class="tab-pane tabHeight-e" id="saved">
                <p class="alert1-e">You have set up 0 alerts for new pets.</p>
                <p class="alert2-e">Pet search alerts monitor for you, so you can get a first look at any newly
                    added pets.</p>
                <p>To set one up, choose 'SET ALERT' next to the bell icon in your search results.</p>
                <p class="alert3-e"><i class="fas fa-angle-left"></i> <a href="#" class="alertPet-e">All (0)</a> <i
                        class="fas fa-angle-right"></i></p>
            </div>
        </div>
    </div>
</div>
<script>
// Show the file chose/image for PET
$(document).on('click', '.browse', function(){
    var file = $(this).parent().parent().parent().find('.file');
    file.trigger('click');
  });
  $(document).on('change', '.file', function(){
    $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
  });
</script>
@stop