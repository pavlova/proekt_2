<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function states()
    {
        return $this->hasMany('App\State');
    }

    public function user_profiles()
    {
        return $this->hasMany('App\User_Profile');
    }
}
