@extends ('layouts/master')

@section('jumbotron')
<!-- JUMBOTRON -->
<div class="container-fluid cov-img">
    <div class='row row5 text-center '>
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            <h1>Where Pets Find Their People</h1>
            <h3>Thousands of adoptable pets are looking for people. People like you.</h3><br><br>
            <a href="{{ route('dashboard') }}" class="btn btn-default btn-b">Find your match!</a>
        </div>
    </div>
</div>
@stop

@section('box-animal')
<!-- BOX-ANIMAL -->
<div class='container container-Box'>
    <div class="box-1-n">
        <a href=''>
            <div class='col-md-4 col-sm-4 displayNone findAPetMenu-locationInputContainer flexDisplay'>
                <p>Newfoundland and Labrador, Canada</p>
            </div>
            <div class="triangle-n"></div>
        </a>
        <a href='{{ route('dashboard') }}'>
            <div class="col-md-4 col-sm-6 col-xs-6 findAPetMenu flexDisplay findAPetMenuBorderLeft">
                <img src="{{ asset ('images/dog.png') }}" alt="" id="icon-dog-cat">
                <span class="boxText">Find a dog</span>
            </div>
        </a>
        <a href='{{ route('dashboard') }}'>
            <div class="col-md-4 col-sm-6 col-xs-6 findAPetMenu  findAPetMenuCat flexDisplay">
                <img src="{{ asset ('images/cat.png') }}" alt="" id="icon-dog-cat">
                <span class="boxText"> Find a cat</span>
            </div>
        </a>
        <a href=''>
            <div class='col-md-4 col-sm-12 col-xs-12 displayNoneLastBox '>
                <p>Newfoundland and Labrador, Canada</p>
            </div>
        </a>
    </div>
</div>
@stop

@section('cards')
<!-- CARDS -->
<div class="container-fluid container-fluid-CARD">
    <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 text-center">
        <p class="h1 subtitle">Pets waiting for adoption</p>
        <!-- FIRST-CARD -->
        @foreach($petsData as $pet)
        <div @if ($loop->first)
            class="col-md-2 col-md-offset-1 col-sm-4 col-xs-12 card recommendations-body-inner"
            @else
            class="col-md-2 col-sm-4 col-xs-12 card recommendations-body-inner"
            @endif
            >
            <div class="flip-card">
                <div class="flip-card-inner">
                    <div class="petCard flip-card-front">
                        <div class="card-img">
                            <img src="{{ URL::to('/images/petImages/'.$pet['image']) }}" alt="" class="imageCARD">
                        </div>
                        <div class="card-body">
                            <h5>{{ $pet['name'] }}</h5>
                        </div>
                    </div>
                    <div class="petCard-hover flip-card-back">
                        <div class="card-img-hover">
                            <img src="{{ URL::to('/images/petImages/'.$pet['image']) }}" class="img-circle">
                            <a class="btn btn-circle" href="{{ route('petprofile', ['id' => $pet['id']]) }}"
                                target="_blank"><i class="fas fa-share"></i></a>
                        </div>
                        <div class="card-body-hover">
                            <h4>{{ $pet['name'] }}</h4>
                            <h5>{{ $pet['type'] }}</h5>
                            <h5>{{ $pet['size'] }} {{ $pet['gender'] }}</h5>
                            <h5>{{ $pet['location'] }}</h5>
                        </div>
                        <div class="card-body-active">
                            <h3 class="pet-name-h3"><a href="{{ route('petprofile', ['id' => $pet['id']]) }}"
                                    target="_blank">{{ $pet['name'] }} </a></h3>
                            <a rel="nofollow" href="http://www.facebook.com/share.php?u=<;url>"
                                onclick="return fbs_click()" target="_blank" class="fb_share_link"><i
                                    class="fab fa-facebook-f"></i></a>
                            <a
                                href="https://twitter.com/home?status=This%20is%20Awesome%20page!!%20http%3A//yourpage.com"><i
                                    class="fab fa-twitter" target="_blank"></i></a>
                            <a href="http://www.pinterest.com/pin/create/button/?url=<page url (encoded)>&media=<picture url (encoded)>&description=<HTML-encoded description>"
                                target="_blank" class="pinterest-anchor pinterest-hidden"><i
                                    class="fab fa-pinterest-p"></i></a>
                            <a href="mailto:abc@xyz.com?&subject=Your Subject&body=your%20mail%20content"><i
                                    class="fas fa-envelope"></i></a>
                            <a role="button" class="copyToClipboard"
                                data-target="{{ route('petprofile', ['id' => $pet['id']]) }}"><i
                                    class="fas fa-link"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        <!-- FIRST-CARD-FINISH-->
        <!-- FIFTH -CARD -->
        <div class="col-md-2  col-sm-4 col-xs-12 card recommendations-body-inner">
            <div class="flip-card clickable">
                <a href="{{ route('dashboard') }}"></a>
                <div class="flip-card-inner">
                    <div class="petCard flip-card-front last-pet-card">
                        <div class="card-img last-card">
                            <i class="fas fa-paw"></i>
                            <h5>{{ $remainingPets }} more pets avalible</h5>
                        </div>
                        <div class="card last-card-body">
                            <h5>MET THEM</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- FIFTH-CARD-FINISH-->
    </div>
</div>
@stop

@section('story-guide')
<!-- STORY GUIDE -->
<!-- lap-top-version -->
<div class="container storyGuide-L">
    <p class="h1 text-center">Planning to Adopt a Pet?</p>
    <div class="col-md-4">
        <div class="card-storyGuide">
            <img alt="" class="lazyLoad-img" animation-class="m-lazyLoad-img_reveal"
                src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/08/PCE_Blue_Icon-18.png" aria-hidden="true"
                style="height: 124px;">
            <p class="h3 h3_color_b">Checklist for New Adopters</p>
            <p class='text-muted'>Help make the transition, as smooth as possible.</p>
            <a href="https://www.petfinder.com/pet-adoption/dog-adoption/pet-adoption-checklist/" class="btn btn-default btn-storyGuide">Learn more</a>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card-storyGuide">
            <img alt="" class="lazyLoad-img" animation-class="m-lazyLoad-img_reveal"
                src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/08/PCE_Blue_Icon-17.png" aria-hidden="true">
            <p class="h3 h3_color_b">Finding the Right Pet</p>
            <p class='text-muted'>Get tips on figuring out who you should adopt..</p>
            <a href="https://www.petfinder.com/blog/2015/03/4-tips-for-finding-the-right-pet-for-you/" class="btn btn-default btn-storyGuide">Learn more</a>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card-storyGuide">
            <img alt="" class="lazyLoad-img" animation-class="m-lazyLoad-img_reveal"
                src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/08/PCE_Blue_Icon-8.png" aria-hidden="true">
            <p class="h3 h3_color_b">Pet Adoption FAQs</p>
            <p class='text-muted'>Get answers to questions you haven't thought of.</p>
            <a href="https://www.petfinder.com/pet-adoption/pet-adoption-information/pet-adoption-faqs/" class="btn btn-default btn-storyGuide">Learn more</a>
        </div>
    </div>
</div>
<!-- lap-top-version-finish -->
<!-- tablet-version -->
<div class="container storyGuide-TM">
    <p class="h1 text-center">Planning to Adopt a Pet?</p>
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div class="card-storyGuide">
                    <img alt="" class="lazyLoad-img" animation-class="m-lazyLoad-img_reveal"
                        src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/08/PCE_Blue_Icon-18.png"
                        aria-hidden="true" style="height: 124px;">
                    <p class="h3 h3_color_b">Checklist for New Adopters</p>
                    <p class='text-muted'>Help make the transition, as smooth as possible.</p>
                    <a href="" class="btn btn-storyGuide-TM">Learn more</a>
                </div>
            </div>
            <div class="item">
                <div class="card-storyGuide">
                    <img alt="" class="lazyLoad-img" animation-class="m-lazyLoad-img_reveal"
                        src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/08/PCE_Blue_Icon-17.png"
                        aria-hidden="true">
                    <p class="h3 h3_color_b">Finding the Right Pet</p>
                    <p class='text-muted'>Get tips on figuring out who you should adopt..</p>
                    <a href="" class="btn btn-storyGuide-TM">Learn more</a>
                </div>
            </div>
            <div class="item">
                <div class="card-storyGuide">
                    <img alt="" class="lazyLoad-img" animation-class="m-lazyLoad-img_reveal"
                        src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/08/PCE_Blue_Icon-8.png"
                        aria-hidden="true">
                    <p class="h3 h3_color_b">Pet Adoption FAQs</p>
                    <p class='text-muted'>Get answers to questions you haven't thought of.</p>
                    <a href="" class="btn btn-storyGuide-TM">Learn more</a>
                </div>
                <div class="petCard-hover flip-card-back">
                    <div class="card-img-hover">
                        <img src="https://i.pinimg.com/736x/1e/53/f4/1e53f4e398a2c278f4f560ff37b3473d--fluffy-kittens-fluffy-cat.jpg"
                            class="img-circle">
                    </div>
                    <div class="card-body-hover">
                        <h4>Mapple</h4>
                        <h5>Tabby</h5>
                        <h5>Young Male</h5>
                        <h5>Fredericton, NB</h5>
                        <div class="card-body-active">
                            <h3>Mapple Bella</h3>
                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                            <a href="#"><i class="fab fa-twitter"></i></a>
                            <a href="#"><i class="fab fa-pinterest-p"></i></a>
                            <a href="#"><i class="fas fa-envelope"></i></a>
                            <a href="#"><i class="fas fa-link"></i></a>
                        </div>
                    </div>
                </div>

            </div>
            <!-- <div class="carousel-caption"></div> -->
        </div>
    </div>
    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<!-- tablet-version-finish -->
<!-- STORY GUIDE FINISH -->
@stop

@section('recommended-cards')
<!-- RECOMMENDATIOS-VIEWED-PETS -->
<div class="container-fluid container-fluid-VIEWD-PETS">
    <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 text-center">
        <p class="h1 subtitle-view-pets">RECENTLY VIEWD PETS</p>
        <!-- FIRST-CARD -->
        @foreach($petsData1 as $pet)
        <div @if ($loop->first)
            class="col-md-2 col-md-offset-1 col-sm-4 col-xs-12 card recommendations-body-inner"
            @else
            class="col-md-2 col-sm-4 col-xs-12 card recommendations-body-inner"
            @endif
            >
            <div class="flip-card">
                <div class="flip-card-inner">
                    <div class="petCard flip-card-front">
                        <div class="card-img">
                            <img src="{{ URL::to('/images/petImages/'.$pet['image']) }}" alt="" class="imageCARD">
                        </div>
                        <div class="card-body">
                            <h5>{{ $pet['name'] }}</h5>
                        </div>
                    </div>
                    <div class="petCard-hover flip-card-back">
                        <div class="card-img-hover">
                            <img src="{{ URL::to('/images/petImages/'.$pet['image']) }}" class="img-circle">
                            <a class="btn btn-circle" href="{{ route('petprofile', ['id' => $pet['id']]) }}"
                                target="_blank"><i class="fas fa-share"></i></a>
                        </div>
                        <div class="card-body-hover">
                            <h4>{{ $pet['name'] }}</h4>
                            <h5>{{ $pet['type'] }}</h5>
                            <h5>{{ $pet['size'] }} {{ $pet['gender'] }}</h5>
                            <h5>{{ $pet['location'] }}</h5>
                        </div>
                        <div class="card-body-active">
                            <h3 class="pet-name-h3"><a href="{{ route('petprofile', ['id' => $pet['id']]) }}"
                                    target="_blank">{{ $pet['name'] }} </a></h3>
                            <a rel="nofollow" href="http://www.facebook.com/share.php?u=<;url>"
                                onclick="return fbs_click()" target="_blank" class="fb_share_link"><i
                                    class="fab fa-facebook-f"></i></a>
                            <a
                                href="https://twitter.com/home?status=This%20is%20Awesome%20page!!%20http%3A//yourpage.com"><i
                                    class="fab fa-twitter" target="_blank"></i></a>
                            <a href="http://www.pinterest.com/pin/create/button/?url=<page url (encoded)>&media=<picture url (encoded)>&description=<HTML-encoded description>"
                                target="_blank" class="pinterest-anchor pinterest-hidden"><i
                                    class="fab fa-pinterest-p"></i></a>
                            <a href="mailto:abc@xyz.com?&subject=Your Subject&body=your%20mail%20content"><i
                                    class="fas fa-envelope"></i></a>
                            <a role="button" class="copyToClipboard"
                                data-target="{{ route('petprofile', ['id' => $pet['id']]) }}"><i
                                    class="fas fa-link"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        <!-- FIRST-CARD-FINISH-->
        <!-- FIFTH -CARD -->
        <div class="col-md-2  col-sm-4 col-xs-12 card recommendations-body-inner">
            <div class="flip-card clickable">
                <a href="{{ route('dashboard') }}"></a>
                <div class="flip-card-inner">
                    <div class="petCard flip-card-front last-pet-card">
                        <div class="card-img last-card">
                            <i class="fas fa-paw"></i>
                            <h5>{{ $remainingPets }} more pets avalible</h5>
                        </div>
                        <div class="card last-card-body">
                            <h5>MET THEM</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- FIFTH-CARD-FINISH-->
    </div>
</div>
@stop

@section('big-cards')
<div class="container-fluid container-fluid-BIG-CARDS">
    <!-- big-cards -->
    <div class="container">
        <div class="rowBigCards">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="Big-Card">
                    <div class="Big-Card-content">
                        <div class="Big-Card-Img">
                            <img src="https://www.mlar.org/media/1165/dustin-1.jpg" alt="">
                        </div>
                        <div class="Big-Card-Body">
                            <div class="Big-Card-media-badge">
                                <img class="img-circle" src="https://www.mlar.org/media/1165/dustin-1.jpg" alt="">
                                <h3>Cat Adoption Articles</h3>
                                <h4>Helpful insights on what to expect</h5>
                            </div>
                        </div>
                        <a href="https://www.petfinder.com/pet-adoption/cat-adoption/" class="btn btn-Big-Card-Button">READ MORE</a>
                    </div>
                </div>
            </div>
            <!-- FIRST-BIG-CARD-FINISHED -->
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="Big-Card">
                    <div class="Big-Card-content">
                        <div class="Big-Card-Img">
                            <img src="http://petstime.ru/sites/default/files/white-shepperd3.jpg" alt="">
                        </div>
                        <div class="Big-Card-Body">
                            <div class="Big-Card-media-badge">
                                <img class="img-circle" src="http://petstime.ru/sites/default/files/white-shepperd3.jpg"
                                    alt="">
                                <h3>Dog Adoption Articles</h3>
                                <h4>Helpful insights on what to expect</h5>
                            </div>
                        </div>
                        <a href="https://www.petfinder.com/pet-adoption/dog-adoption/" class="btn btn-Big-Card-Button">READ MORE</a>
                    </div>
                </div>
            </div>
            <!-- SECOND-BIG-CARD-FINISHED -->
        </div>
    </div>
    <!-- big-cards-finish -->
</div>
@stop