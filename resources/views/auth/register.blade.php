@if (count($errors) > 0 && !$errors->has('email') && !$errors->has('password'))
<script>
    $(document).ready(function () {
        $('#myModal1').modal('show');
    });
</script>
@endif

<div id="myModal1" class="modal fade fade-backColor-e">
    <div class="modal-dialog modal-lgn" role="document">
        <div class="modal-content">
            <div class="container-fluid p0-e">
                <div class="col-md-12">
                    <div class="modal-header modal-header-e p0-e">
                        <button type="button" class="close close-modal-e" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                </div>
                <div class="displayFlex-e">
                    <div class="box1-e center-e">
                        <div class="widthRegister1-e">
                            <h2 class="register-e">Register</h2>
                        </div>
                    </div>
                </div>
                <form method="POST" action="{{ route('register.post') }}">
                    <div class="displayFlex-e">
                        <div class="box1-e center-e">
                            <div class="form-group form-group-e widthRegister1-e">
                                @foreach($errors->get('firstname') as $error)
                                <p class="error">{{ $error }}</p>
                                @endforeach
                                <span>First Name</span>
                                <input type="text" class="form-control input-e" name="firstname" autocomplete="off"
                                    value="{{ old('firstname') }}">
                            </div>
                        </div>
                        <div class="box2-e center-e">
                            <div class="form-group form-group-e widthRegister2-e">
                                @foreach($errors->get('lastname') as $error)
                                <p class="error">{{ $error }}</p>
                                @endforeach
                                <span>Last Name</span>
                                <input type="text" class="form-control input-e" name="lastname" autocomplete="off"
                                    value="{{ old('lastname') }}">
                            </div>
                        </div>
                    </div>
                    <div class="displayFlex-e">
                        <div class="box1-e center-e">
                            <div class="form-group form-group-e widthRegister1-e">
                                @foreach($errors->get('country') as $error)
                                <p class="error">{{ $error }}</p>
                                @endforeach
                                <span>Country</span>
                                <select class="form-control input-e" name="country">
                                    <option value="">Choose country</option>
                                    @foreach ($countries as $country)
                                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="box2-e center-e">
                            <div class="form-group form-group-e widthRegister2-e">
                                @foreach($errors->get('postalCode') as $error)
                                <p class="error">{{ $error }}</p>
                                @endforeach
                                <span>Postal Code</span>
                                <input type="text" class="form-control input-e" name="postalCode" autocomplete="off"
                                    value="{{ old('postalCode') }}">
                            </div>
                        </div>
                    </div>
                    <div class="displayFlex-e">
                        <div class="box1-e center-e">
                            <div class="form-group form-group-e widthRegister1-e">
                                @foreach($errors->get('emailR') as $error)
                                <p class="error">{{ $error }}</p>
                                @endforeach
                                <span>Email</span>
                                <input type="email" class="form-control input-e" name="emailR" autocomplete="off"
                                    value="{{ old('emailR') }}">
                            </div>
                        </div>
                        <div class="box2-e center-e">
                            <div class="widthRegister2-e">
                                <i>
                                    <p>You will use your email<br>
                                        address to log in</p>
                                </i>
                            </div>
                        </div>
                    </div>
                    <div class="displayFlex-e">
                        <div class="box1-e center-e">
                            <div class="form-group form-group-e widthRegister1-e">
                                @foreach($errors->get('passwordR') as $error)
                                <p class="error">{{ $error }}</p>
                                @endforeach
                                <span>Password</span>
                                <input type="password" class="form-control input-e" name="passwordR" autocomplete="off"
                                    value="{{ old('passwordR') }}">
                            </div>
                        </div>
                        <div class="box2-e center-e">
                            <div class="form-group form-group-e widthRegister2-e">
                                @foreach($errors->get('passwordR_confirmation') as $error)
                                <p class="error">{{ $error }}</p>
                                @endforeach
                                <span>Confirm Password</span>
                                <input type="password" class="form-control input-e" name="passwordR_confirmation"
                                    placeholder="Confirm" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="displayFlex-e">
                        <div class="col-md-6 btn-submit-e">
                            <button type="submit" class="btn btn-modal2-e btn-block btnBottom-e">REGISTER</button>
                        </div>
                    </div>
                    @csrf
                </form>
            </div>
        </div>
    </div>
</div>