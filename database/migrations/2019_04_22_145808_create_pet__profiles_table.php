<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePetProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pet_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pet_name');
            $table->string('location');
            $table->string('img_path');
            $table->string('short_desc');
            $table->string('characteristics');
            $table->boolean('house_trained');
            $table->string('good_in_home');
            $table->mediumText('long_desc');
        
            $table->unsignedBigInteger('type_id');
            $table->foreign('type_id')->references('id')->on('types');

            $table->unsignedBigInteger('coat_length_id');
            $table->foreign('coat_length_id')->references('id')->on('coat_lengths');

            $table->unsignedBigInteger('size_id');
            $table->foreign('size_id')->references('id')->on('sizes');

            $table->unsignedBigInteger('gender_id');
            $table->foreign('gender_id')->references('id')->on('genders');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pet__profiles');
    }
}
