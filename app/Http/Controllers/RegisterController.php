<?php

namespace App\Http\Controllers;

use App\Models\Size;
use App\Models\Type;
use App\Models\State;
use App\Models\Gender;
use App\Models\Country;
use App\Models\Coat_Length;
use App\Models\Pet_Profile;
use App\Models\User_Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\RegisterValidationRequest;
use App\Http\Requests\MyProfileValidationRequest;

class RegisterController extends Controller
{
    public function sendRegisterPost(RegisterValidationRequest $request)
    {
        session()->put(["firstname"=>"$request->firstname", "lastname"=>"$request->lastname", "country"=>"$request->country", "postalCode"=>"$request->postalCode", "emailR"=>"$request->emailR",
        "passwordR"=>"$request->passwordR", "passwordR_confirmation"=>"$request->passwordR_confirmation"]);

        return redirect()->route('myprofile');
    }

    public function sendMyProfilePost(MyProfileValidationRequest $request)
    {
        if (session('id')) {
            $user = User_Profile::findOrFail(session('id'));
        } else {
            $user = new User_Profile();
        }

        $user->first_name = $request->firstMyProfile;
        $user->last_name = $request->lastMyProfile;
        $user->phone_number = $request->phoneMyProfile;
        $user->street_address = $request->streetMyProfile;
        $user->street_num = $request->streetMyProfileC;
        $user->city = $request->cityMyProfile;
        $user->zip_code = $request->zipCodeMyProfile;
        $user->postal_code = session('postalCode');
        $user->email = session('emailR');
        $user->password = session('passwordR');
        $user->is_guest = 0;
        $user->country_id = $request->countryMyProfile;
        $user->state_id = $request->stateMyProfile;
        $user->save();
 
        $pet = new Pet_Profile();

        if (session('id')) {
            $pet->user_profile_id = session('id');
        } else {
            $pet->user_profile_id = $user->id;
        }

        $pet->pet_name = $request->petName;
        $pet->location = $request->petLocation;

        $imageName = time().'.'.$request->petImage->getClientOriginalExtension();
        $request->petImage->move(public_path('images/petImages'), $imageName);
        $pet->img_path = $imageName;
        
        $pet->short_desc = $request->petShortDesc;
        $pet->characteristics = $request->petCharacteristics;
        $pet->house_trained = $request->petHouseTrained;
        $pet->good_in_home = $request->petGoodInHome;
        $pet->long_desc = $request->petLongDesc;
        $pet->type_id = $request->petType;
        $pet->coat_length_id = $request->petCoatLength;
        $pet->size_id = $request->petSize;
        $pet->gender_id = $request->petGender;
        $pet->save();

        $id  = $pet->id;

        $email_to = session('emailR');
        $data = ['email_to' => $email_to];

        \Mail::send('emails.send_mail',$data,function($message) use($data) {
            $message->from('eristova5695@gmail.com');
            $message->to($data['email_to']);
            $message->subject("Pet Finder - Brainster2019");
        });

        return redirect()->route('petprofile', compact('id'));
    }
}