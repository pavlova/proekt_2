@if ($errors->has('email') || $errors->has('password') || session()->has('error'))
    <script>
        $( document ).ready(function() {
            $('#myModal').modal('show');
        });
    </script>
@endif

<div id="myModal" class="modal fade fade-backColor-e" role="dialog">
    <div class="modal-dialog modal-lg modal1Dialog-e" role="document">
        <div class="modal-content height-e">
            <div class="container-fluid p0-e">
                <div class="col-md-6 col-md-push-6 col-sm-12 col-xs-12">
                    <div class="modal-header modal-header-e p0-e">
                        <button type="button" class="close close-modal-e" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="col-md-10 col-md-offset-1 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 p1-e">
                        <h2 class="login-e">Log in</h2>
                        <form method="POST" action="{{ route('login.post') }}">
                            @csrf
                            @foreach($errors->get('email') as $error)
                            <p class="error">{{ $error }}</p>
                            @endforeach
                            @if(session()->has('error'))
                                <p class="error">{{ session()->get('error') }}</p>
                            @endif
                            <div class="form-group form-group-e">
                                <span>Email</span>
                                <input type="email" class="form-control input-e" name="email" value="{{ old('email') }}" autocomplete="off">
                            </div>
                            @foreach($errors->get('password') as $error)
                            <p class="error">{{ $error }}</p>
                            @endforeach
                            <div class="form-group form-group-e">
                                <span>Password</span>
                                <input type="password" class="form-control input-e" name="password" autocomplete="off">
                            </div>
                            <button type="submit" class="btn btn-modal2-e btn-block">LOG IN</button>
                        </form>
                        <div class="border-e text-center">
                            <a href="#" class="forgot-e">FORGOT PASSWORD?</a>
                        </div>
                        <div class="text-center">
                            <h5 class="text1-e">DON'T HAVE AN ACCOUNT?</h5>
                            <button type="button" class="btn btn-modal2-e btn-block btnBottom-e" data-toggle="modal"
                                data-target="#myModal1">REGISTER NOW</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-md-pull-6 col-sm-12 col-xs-12 text2-e">
                    <div class="col-md-10 col-md-offset-1 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                        <h2>We Make Adopting Easier</h2>
                        <ul class="ul-e">
                            <li>Create and save your adopter profile.</li>
                            <li>Save and manage your pet searches and email communications.</li>
                            <li>Learn helpful pet care tips and receive expert advice.</li>
                            <li>Get involved and help promote adoptable pets in your area.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>