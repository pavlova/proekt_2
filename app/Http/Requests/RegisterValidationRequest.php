<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterValidationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|alpha|max:25',
            'lastname' => 'required|alpha|max:25',
            'country' => 'required',
            'postalCode' => 'required|regex:/\b\d{5}\b/',
            'emailR' => 'required|email|unique:user_profiles,email',
            'passwordR' => 'required|min:6|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
            'passwordR_confirmation' => 'min:6',
        ];
    }

    public function messages() 
    {
        return [
            'emailR.required' => 'The email field is required.',
            'emailR.email' => 'The email must be a valid email address.',
            'emailR.unique' => 'The email has already been taken.',
            'passwordR.required' => 'The password field is required.',
            'passwordR.min' => 'The password must be at least 6 characters.',
            'passwordR.regex' => 'The password should have at least one Uppercase letter, one numeric value and one special character.',
            'passwordR_confirmation.min' => 'The password must be at least 6 characters.',
            'passwordR.confirmed' => 'The password confirmation does not match.',
            'postalCode.regex' => 'The postal code must be 5 numbers.'
        ];
    }

}
