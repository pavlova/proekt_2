<?php

use Faker\Factory;
use App\Models\Pet_Profile;
use Illuminate\Database\Seeder;

class Pet_profilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $cats = [
            ['pet_name' => $faker->firstNameFemale, 'location' => $faker->streetName, 
            'img_path' => 'cat1.jpeg', 'short_desc' => $faker->text(200), 
            'characteristics' => $faker->text(64), 'house_trained' => rand(0,1),
            'good_in_home' => $faker->text(120), 'long_desc' => $faker->text(600),
            'type_id' => 2, 'coat_length_id' => rand(1,3), 'size_id' => rand(1,3),
            'gender_id' => 2, 'user_profile_id' => rand(1, 10)
            ],
            ['pet_name' => $faker->firstNameMale, 'location' => $faker->streetName, 
            'img_path' => 'cat2.jpeg', 'short_desc' => $faker->text(200), 
            'characteristics' => $faker->text(64), 'house_trained' => rand(0,1),
            'good_in_home' => $faker->text(120), 'long_desc' => $faker->text(600),
            'type_id' => 2, 'coat_length_id' => rand(1,3), 'size_id' => rand(1,3),
            'gender_id' => 1, 'user_profile_id' => rand(1, 10)
            ],
            ['pet_name' => $faker->firstNameFemale, 'location' => $faker->streetName, 
            'img_path' => 'cat3.jpeg', 'short_desc' => $faker->text(200), 
            'characteristics' => $faker->text(64), 'house_trained' => rand(0,1),
            'good_in_home' => $faker->text(120), 'long_desc' => $faker->text(600),
            'type_id' => 2, 'coat_length_id' => rand(1,3), 'size_id' => rand(1,3),
            'gender_id' => 2, 'user_profile_id' => rand(1, 10)
            ],
            ['pet_name' => $faker->firstNameMale, 'location' => $faker->streetName, 
            'img_path' => 'cat4.jpeg', 'short_desc' => $faker->text(200), 
            'characteristics' => $faker->text(64), 'house_trained' => rand(0,1),
            'good_in_home' => $faker->text(120), 'long_desc' => $faker->text(600),
            'type_id' => 2, 'coat_length_id' => rand(1,3), 'size_id' => rand(1,3),
            'gender_id' => 1, 'user_profile_id' => rand(1, 10)
            ],
            ['pet_name' => $faker->firstNameMale, 'location' => $faker->streetName, 
            'img_path' => 'cat5.jpeg', 'short_desc' => $faker->text(200), 
            'characteristics' => $faker->text(64), 'house_trained' => rand(0,1),
            'good_in_home' => $faker->text(120), 'long_desc' => $faker->text(600),
            'type_id' => 2, 'coat_length_id' => rand(1,3), 'size_id' => rand(1,3),
            'gender_id' => 1, 'user_profile_id' => rand(1, 10)
            ]
        ];

        $dogs = [
            ['pet_name' => $faker->firstNameMale, 'location' => $faker->streetName, 
            'img_path' => 'dog1.jpg', 'short_desc' => $faker->text(200), 
            'characteristics' => $faker->text(64), 'house_trained' => rand(0,1),
            'good_in_home' => $faker->text(120), 'long_desc' => $faker->text(600),
            'type_id' => 1, 'coat_length_id' => rand(1,3), 'size_id' => rand(1,3),
            'gender_id' => 1, 'user_profile_id' => rand(1, 10)
            ],
            ['pet_name' => $faker->firstNameFemale, 'location' => $faker->streetName, 
            'img_path' => 'dog2.jpg', 'short_desc' => $faker->text(200), 
            'characteristics' => $faker->text(64), 'house_trained' => rand(0,1),
            'good_in_home' => $faker->text(120), 'long_desc' => $faker->text(600),
            'type_id' => 1, 'coat_length_id' => rand(1,3), 'size_id' => rand(1,3),
            'gender_id' => 2, 'user_profile_id' => rand(1, 10)
            ],
            ['pet_name' => $faker->firstNameFemale, 'location' => $faker->streetName, 
            'img_path' => 'dog3.jpg', 'short_desc' => $faker->text(200), 
            'characteristics' => $faker->text(64), 'house_trained' => rand(0,1),
            'good_in_home' => $faker->text(120), 'long_desc' => $faker->text(600),
            'type_id' => 1, 'coat_length_id' => rand(1,3), 'size_id' => rand(1,3),
            'gender_id' => 2, 'user_profile_id' => rand(1, 10)
            ],
            ['pet_name' => $faker->firstNameMale, 'location' => $faker->streetName, 
            'img_path' => 'dog4.jpg', 'short_desc' => $faker->text(200), 
            'characteristics' => $faker->text(64), 'house_trained' => rand(0,1),
            'good_in_home' => $faker->text(120), 'long_desc' => $faker->text(600),
            'type_id' => 1, 'coat_length_id' => rand(1,3), 'size_id' => rand(1,3),
            'gender_id' => 1, 'user_profile_id' => rand(1, 10)
            ],
            ['pet_name' => $faker->firstNameFemale, 'location' => $faker->streetName, 
            'img_path' => 'dog5.jpg', 'short_desc' => $faker->text(200), 
            'characteristics' => $faker->text(64), 'house_trained' => rand(0,1),
            'good_in_home' => $faker->text(120), 'long_desc' => $faker->text(600),
            'type_id' => 1, 'coat_length_id' => rand(1,3), 'size_id' => rand(1,3),
            'gender_id' => 2, 'user_profile_id' => rand(1, 10)
            ],
        ];

        foreach ($cats as $cat) {
            $pet = new Pet_Profile();
            $pet->pet_name = $cat['pet_name'];
            $pet->location = $cat['location'];
            $pet->img_path = $cat['img_path'];
            $pet->short_desc = $cat['short_desc'];
            $pet->characteristics  = $cat['characteristics'];
            $pet->house_trained  = $cat['house_trained'];
            $pet->good_in_home  = $cat['good_in_home'];
            $pet->long_desc = $cat['long_desc'];
            $pet->type_id = $cat['type_id'];
            $pet->coat_length_id = $cat['coat_length_id'];
            $pet->size_id = $cat['size_id'];
            $pet->gender_id = $cat['gender_id'];
            $pet->user_profile_id = $cat['user_profile_id'];
            $pet->save();
        }

        foreach ($dogs as $dog) {
            $pet = new Pet_Profile();
            $pet->pet_name = $dog['pet_name'];
            $pet->location = $dog['location'];
            $pet->img_path = $dog['img_path'];
            $pet->short_desc = $dog['short_desc'];
            $pet->characteristics  = $dog['characteristics'];
            $pet->house_trained  = $dog['house_trained'];
            $pet->good_in_home  = $dog['good_in_home'];
            $pet->long_desc = $dog['long_desc'];
            $pet->type_id = $dog['type_id'];
            $pet->coat_length_id = $dog['coat_length_id'];
            $pet->size_id = $dog['size_id'];
            $pet->gender_id = $dog['gender_id'];
            $pet->user_profile_id = $dog['user_profile_id'];
            $pet->save();
        }
        
    }
}