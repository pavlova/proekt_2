<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pet_Profile extends Model
{
    protected $table = 'pet_profiles';

    public function coat_lengths()
    {
        return $this->belongsTo('App\Coat_Length');
    }

    public function types()
    {
        return $this->belongsTo('App\Type');
    }

    public function sizes()
    {
        return $this->belongsTo('App\Size');
    }

    public function genders()
    {
        return $this->belongsTo('App\Gender');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function user_profiles()
    {
        return $this->belongsTo('App\User_Profile');
    }
}
