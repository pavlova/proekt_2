<?php

use App\Models\Type;
use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
        $types = ['Dog','Cat'];

        foreach($types as $value)
        {   
            $type = new Type;
            $type->type = $value;
            $type->save();
        }
    }
}
