<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    public function countries()
    {
        return $this->belongsTo('App\Country');
    }

    public function user_profiles()
    {
        return $this->hasMany('App\User_Profile');
    }
}
