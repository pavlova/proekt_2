<?php

namespace App\Http\Controllers;

use App\Models\Size;
use App\Models\Type;
use App\Models\State;
use App\Models\Gender;
use App\Models\Country;
use App\Models\Coat_Length;
use App\Models\Pet_Profile;
use Illuminate\Http\Request;


class PetFinderController extends Controller
{
    public function showHomePage()
    {
        $page_title = 'Home-Page';
        $countries = Country::all();
        $pets = Pet_Profile::all()->random(4);
        $pets1 = Pet_Profile::all()->random(4);
        
        $petsData = self::getPetData($pets);
        $petsData1 = self::getPetData($pets1);

        $all = Pet_Profile::all()->count();
        $remainingPets  = $all - 4;

        return view('index', compact('page_title', 'countries', 'petsData', 'petsData1', 'remainingPets'));
    }

    public static function getPetData($petArr) 
    {
        $petData = [];
        $petId = $petName = $petType = $petSize = $petGender = $petLocation = $petImage = "";

        foreach ($petArr as $pet) {
            $petId = $pet->id;
            $petName = $pet->pet_name;
            $petImage = $pet->img_path;
            $petLocation = $pet->location;

            $petTypeArr = Type::where('id', $pet->type_id)->get('type');
            foreach ($petTypeArr as $type) {
                $petType = $type['type'];
            }
            $petSizeArr = Size::where('id', $pet->size_id)->get('pet_size');
            foreach ($petSizeArr as $size) {
                $petSize = $size['pet_size'];
            }
            $petGenderArr = Gender::where('id', $pet->gender_id)->get('gender');
            foreach ($petGenderArr as $gender) {
                $petGender = $gender['gender'];
            }

            $petData[] = [
                'id' => $petId, 'name' => $petName, 'type' => $petType, 'size' => $petSize, 
                'gender' => $petGender, 'location' => $petLocation, 'image' => $petImage
            ]; 
        }

        return $petData;
    }

    public function showMyProfile()
    {
        $page_title = 'My Profile';
        $countries = Country::all();
        $states = State::all();
        $types = Type::all();
        $coat_lengths = Coat_Length::all();
        $sizes = Size::all();
        $genders = Gender::all();

        return view('myprofile', compact('page_title', 'countries', 'states', 'types', 'coat_lengths', 'sizes', 'genders'));
    }

    public function showPetProfile($id)
    {
        $pet = Pet_Profile::findOrFail($id);

        $pet_profile = [];
        $pet_type = $pet_size = $pet_gender = $pet_coat = $pet_house = '';

        $typeArr = Type::where('id', $pet->type_id)->get('type');
        foreach ($typeArr as $type) {
            $pet_type = $type['type'];
        }

        $sizeArr = Size::where('id', $pet->size_id)->get('pet_size');
        foreach ($sizeArr as $size) {
            $pet_size = $size['pet_size'];
        }

        $genderArr = Gender::where('id', $pet->gender_id)->get('gender');
        foreach ($genderArr as $gender) {
            $pet_gender = $gender['gender'];
        }

        $coatLengthArr = Coat_Length::where('id', $pet->coat_length_id)->get('coat_length');
        foreach ($coatLengthArr as $coatLength) {
            $pet_coat = $coatLength['coat_length'];
        }

        if ($pet->house_traind === 1) {
            $pet_house = 'Yes';
        } else {
            $pet_house = 'No';
        }

        $pet_profile = ['name' => $pet->pet_name, 'img' => $pet->img_path, 
        'type' => $pet_type, 'location' => $pet->location, 'size' => $pet_size,
        'gender' => $pet_gender, 'short_desc' => $pet->short_desc, 
        'characteristics' => $pet->characteristics, 
        'coat' => $pet_coat, 'house' => $pet_house, 'good_in_home' => $pet->good_in_home,
        'long_desc' => $pet->long_desc];

        $page_title = 'Pet Profile';
        $countries = Country::all();

        $pets = Pet_Profile::all()->random(4);
        $petsData = self::getPetData($pets);
        
        $all = Pet_Profile::all()->count();
        $remainingPets  = $all - 4;

        return view('petprofile', compact('pet_profile', 'page_title', 'countries', 'petsData', 'remainingPets'));
    }
    
}