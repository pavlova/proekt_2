<?php

use App\Models\Size;
use Illuminate\Database\Seeder;

class SizeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sizes = ['Small','Medium','Large'];

        foreach($sizes as $value)
        {   
            $size = new Size;
            $size->pet_size = $value;
            $size->save();
        }
    }
}
