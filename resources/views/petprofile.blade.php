@extends ('layouts/master')

@section('petImage')
<div class="container-fluid petDiv1-e imgCenter-e p0-e">
    <div class="col-md-5 imgCenter-e">
        <img id="petImgModal" src="{{ URL::to('/images/petImages/'.$pet_profile['img']) }}"
            class="img-responsive petPicture-e" alt="Pet" />
    </div>
</div>
@stop

@section('petInfo')
<div class="container-fluid petDiv2-e petBoxes-e p0-e">
    <div class="col-md-6 col-xs-12 petBox1-e">
        <div class="col-md-12">
            <h1 class="petName1-e">{{ $pet_profile['name'] }}</h1>
            <ul class="list1-e">
                <li>{{ $pet_profile['type'] }}</li>
                <li>{{ $pet_profile['location'] }}</li>
            </ul>
            <div class="hr"></div>
        </div>
        <div class="col-md-12">
            <ul class="list2-e">
                <li>{{ $pet_profile['size'] }}</li>
                <li>{{ $pet_profile['gender'] }}</li>
            </ul>
            <div class="hr"></div>
        </div>
        <div class="col-md-12">
            <h2 class="petName1-e">About</h2>
            <h4 class="pet2-e">SHORT DESCRIPTION</h4>
            <h5>{{ $pet_profile['short_desc'] }}</h5>
            <h4 class="pet2-e">CHARACTERISTICS</h4>
            <h5>{{ $pet_profile['characteristics'] }}</h5>
            <h4 class="pet2-e">COAT LENGTH</h4>
            <h5>{{ $pet_profile['coat'] }}</h5>
            <h4 class="pet2-e">HOUSE-TRAINED</h4>
            <h5>{{ $pet_profile['house'] }}</h5>
            <h4 class="pet2-e">GOOD IN A HOME WITH</h4>
            <h5 class="pet3-e">{{ $pet_profile['good_in_home'] }}</h5>
            <div class="hr"></div>
        </div>
        <div class="col-md-12 pet3-e">
            <h2 class="petName1-e">Meet {{ $pet_profile['name'] }}</h2>
            <p>{{ $pet_profile['long_desc'] }}</p>
        </div>
    </div>
    <div class="col-md-3 col-xs-12 petBox2-e p0-e">
        <div class="col-md-12">
            <a role="button" href="#askAbout" class="btn btnAskAbout-e btn-block btnBottom-e">ASK ABOUT
                {{ $pet_profile['name'] }}</a>
        </div>
        <div class="col-md-12 p0-e">
            <div class="col-md-6 p0-e">
                <a role="button" rel="nofollow" href="http://www.facebook.com/share.php?u=<;url>"
                    onclick="return fbs_click()" target="_blank" class="btn btnAskAbout1-e btn-block"><i
                        class="fas fa-share"></i>
                    SHARE</a>
            </div>
            <div class="col-md-6 p0-e">
                <a role="button" class="btn btnAskAbout2-e btn-block"><i class="fas fa-print"></i> PRINT</a>
            </div>
        </div>
    </div>
</div>
@stop

@section('askAboutPet')
<div class="container-fluid petDiv-n" id="askAbout">
    <div class="col-sm-10 col-sm-offset-1 petDiv2-n">
        <div class="col-sm-12 About-n">
            <img src="https://dl5zpyw5k3jeb.cloudfront.net/photos/pets/44452427/1/?bust=1555122147&width=560" alt=""
                class="petCirclePic-n">
            <div class="AboutPet-n">
                <h4 class="petText-n">Ask About {{ $pet_profile['name'] }}</h4>
                <p>{{ $pet_profile['location'] }}</p>
                <ul class="petDesc-n">
                    <li>{{ $pet_profile['gender'] }}</li>
                    <li>{{ $pet_profile['size'] }}</li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12 toPet-n">
            <h5>TO</h5>
            <p>Good Bones {{ $pet_profile['type'] }} Rescue</p>
        </div>
    </div>
</div>
@stop

@section('guestForm')
<div class="container-fluid petDiv3-n">
    <div class="col-sm-10 col-sm-offset-1 petDiv4-n">
        <div class="col-xs-12">
            <h6>Have an acount?<a href=""> Sign in</a></h6>
        </div>
        <div class="col-sm-6">
            <h5><b>OR INQUIRE AS A GUEST</b></h5>
            <form method="" action="">
                <div class="container-fluid p0-e">
                    <div class="col-md-6 form-group-n">
                        <div class="form-group form-group-e">
                            <span>First name</span>
                            <input type="text" class="form-control input-e" name="firstMyProfile" value=""
                                autocomplete="off">
                        </div>
                    </div>
                    <div class="col-md-6 form-group-n">
                        <div class="form-group form-group-e">
                            <span>Last name</span>
                            <input type="text" class="form-control input-e" name="lastMyProfile" value=""
                                autocomplete="off">
                        </div>
                    </div>
                    <div class="col-md-6 form-group-n">
                        <div class="form-group form-group-e">
                            <span>Email</span>
                            <input type="email" class="form-control input-e" name="emailMyProfile" value=""
                                autocomplete="off">
                        </div>
                    </div>
                    <div class="col-md-6 form-group-n">
                        <div class="form-group form-group-e">
                            <span>Phone number (Optional)</span>
                            <input type="text" class="form-control input-e" name="phoneMyProfile" value=""
                                autocomplete="off">
                        </div>
                    </div>
                    <div class="col-md-6 form-group-n">
                        <div class="form-group form-group-e inputBottom-e">
                            <span>Country (required)</span>
                            <select class="form-control input-e" name="countryMyProfile">
                                <option value="">Choose country</option>
                                <option value="">Macedonia</option>
                                <option value="">United States</option>
                                <option value="">Canada</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 form-group-n">
                        <div class="form-group form-group-e">
                            <span>Postal code</span>
                            <input type="text" class="form-control input-e" name="zipCodeMyProfile" value=""
                                autocomplete="off" placeholder="">
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-6 form-group-n">
            <h5><b>YOUR MESSAGE (5000 CHARACTER LIMIT) </b></h5>
            <div class="col-md-12 p0-e">
                <div class="form-group">
                    <textarea class="form-control inputPet-e inputPet-n" name="petLongDesc" rows="8"></textarea>
                </div>
            </div>
            <div class="col-md-12 btn-submit-n">
                <button type="submit" class="btn btn-modal2-e btn-modal2-n btn-block">SEND MESSAGE</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('recommended-cards')
<!-- RECOMMENDATIOS-VIEWED-PETS -->
<div class="container-fluid container-fluid-VIEWD-PETS">
    <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 text-center">
        <p class="h1 subtitle-view-pets">RECENTLY VIEWD PETS</p>
        <!-- FIRST-CARD -->
        @foreach($petsData as $pet)
        <div @if ($loop->first)
            class="col-md-2 col-md-offset-1 col-sm-4 col-xs-12 card recommendations-body-inner"
            @else
            class="col-md-2 col-sm-4 col-xs-12 card recommendations-body-inner"
            @endif
            >
            <div class="flip-card">
                <div class="flip-card-inner">
                    <div class="petCard flip-card-front">
                        <div class="card-img">
                            <img src="{{ URL::to('/images/petImages/'.$pet['image']) }}" alt="" class="imageCARD">
                        </div>
                        <div class="card-body">
                            <h5>{{ $pet['name'] }}</h5>
                        </div>
                    </div>
                    <div class="petCard-hover flip-card-back">
                        <div class="card-img-hover">
                            <img src="{{ URL::to('/images/petImages/'.$pet['image']) }}" class="img-circle">
                            <a class="btn btn-circle" href="{{ route('petprofile', ['id' => $pet['id']]) }}"
                                target="_blank"><i class="fas fa-share"></i></a>
                        </div>
                        <div class="card-body-hover">
                            <h4>{{ $pet['name'] }}</h4>
                            <h5>{{ $pet['type'] }}</h5>
                            <h5>{{ $pet['size'] }} {{ $pet['gender'] }}</h5>
                            <h5>{{ $pet['location'] }}</h5>
                        </div>
                        <div class="card-body-active">
                            <h3 class="pet-name-h3"><a href="{{ route('petprofile', ['id' => $pet['id']]) }}"
                                    target="_blank">{{ $pet['name'] }} </a></h3>
                            <a rel="nofollow" href="http://www.facebook.com/share.php?u=<;url>"
                                onclick="return fbs_click()" target="_blank" class="fb_share_link"><i
                                    class="fab fa-facebook-f"></i></a>
                            <a
                                href="https://twitter.com/home?status=This%20is%20Awesome%20page!!%20http%3A//yourpage.com"><i
                                    class="fab fa-twitter" target="_blank"></i></a>
                            <a href="http://www.pinterest.com/pin/create/button/?url=<page url (encoded)>&media=<picture url (encoded)>&description=<HTML-encoded description>"
                                target="_blank" class="pinterest-anchor pinterest-hidden"><i
                                    class="fab fa-pinterest-p"></i></a>
                            <a href="mailto:abc@xyz.com?&subject=Your Subject&body=your%20mail%20content"><i
                                    class="fas fa-envelope"></i></a>
                            <a role="button" class="copyToClipboard"
                                data-target="{{ route('petprofile', ['id' => $pet['id']]) }}"><i
                                    class="fas fa-link"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        <!-- FIRST-CARD-FINISH-->
        <!-- FIFTH -CARD -->
        <div class="col-md-2  col-sm-4 col-xs-12 card recommendations-body-inner">
            <div class="flip-card clickable">
                <a href="{{ route('dashboard') }}"></a>
                <div class="flip-card-inner">
                    <div class="petCard flip-card-front last-pet-card">
                        <div class="card-img last-card">
                            <i class="fas fa-paw"></i>
                            <h5>{{ $remainingPets }} more pets avalible</h5>
                        </div>
                        <div class="card last-card-body">
                            <h5>MET THEM</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- FIFTH-CARD-FINISH-->
    </div>
</div>
@stop

@section('big-cards')
<div class="container-fluid container-fluid-BIG-CARDS">
    <!-- big-cards -->
    <div class="container">
        <div class="rowBigCards">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="Big-Card">
                    <div class="Big-Card-content">
                        <div class="Big-Card-Img">
                            <img src="https://www.mlar.org/media/1165/dustin-1.jpg" alt="">
                        </div>
                        <div class="Big-Card-Body">
                            <div class="Big-Card-media-badge">
                                <img class="img-circle" src="https://www.mlar.org/media/1165/dustin-1.jpg" alt="">
                                <h3>Cat Adoption Articles</h3>
                                <h4>Helpful insights on what to expect</h5>
                            </div>
                        </div>
                        <a href="https://www.petfinder.com/pet-adoption/cat-adoption/" class="btn btn-Big-Card-Button">READ MORE</a>
                    </div>
                </div>
            </div>
            <!-- FIRST-BIG-CARD-FINISHED -->
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="Big-Card">
                    <div class="Big-Card-content">
                        <div class="Big-Card-Img">
                            <img src="http://petstime.ru/sites/default/files/white-shepperd3.jpg" alt="">
                        </div>
                        <div class="Big-Card-Body">
                            <div class="Big-Card-media-badge">
                                <img class="img-circle" src="http://petstime.ru/sites/default/files/white-shepperd3.jpg"
                                    alt="">
                                <h3>Dog Adoption Articles</h3>
                                <h4>Helpful insights on what to expect</h5>
                            </div>
                        </div>
                        <a href="https://www.petfinder.com/pet-adoption/dog-adoption/" class="btn btn-Big-Card-Button">READ MORE</a>
                    </div>
                </div>
            </div>
            <!-- SECOND-BIG-CARD-FINISHED -->
        </div>
    </div>
    <!-- big-cards-finish -->
</div>
@stop

@section('modalForPetImage')
<div id="myModalPet" class="modal modalPet-e">
    <span class="closeBtn-e">&times;</span>
    <img class="modal-content modal-contentPet-e" id="imgPet">
</div>
@stop