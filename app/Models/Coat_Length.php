<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coat_Length extends Model
{
    protected $table = 'coat_lengths';

    public function pet_profiles()
    {
        return $this->hasMany('App\Pet_Profile');
    }

}
