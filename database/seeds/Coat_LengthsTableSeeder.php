<?php

use App\Models\Coat_Length;
use Illuminate\Database\Seeder;

class Coat_LengthsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $coat_lengths = ['Short','Medium','Long'];

        foreach($coat_lengths as $value)
        {   
            $coat_length = new Coat_Length;
            $coat_length->coat_length = $value;
            $coat_length->save();
        }
    }
}
