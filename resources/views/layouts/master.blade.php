<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
        integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css"
        integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset ('css/style.css') }}">

    <title>{{ $page_title }}</title>
</head>

<body>

    @if ($page_title === 'Home-Page' || $page_title === 'Pet Profile' || $page_title === 'Dashboard')
    @include('layouts/header')
    @endif

    <!-- Dashboard Sections -->
    @hasSection('search')
    @yield('search')
    @endif
    <!-- End Dashboard Sections -->

    <!-- Home Page Sections -->
    @hasSection('jumbotron')
    @yield('jumbotron')
    @endif

    @hasSection('box-animal')
    @yield('box-animal')
    @endif

    @hasSection('cards')
    @yield('cards')
    @endif

    @hasSection('story-guide')
    @yield('story-guide')
    @endif

    @if ($page_title === 'Home-Page')
    @hasSection('recommended-cards')
    @yield('recommended-cards')
    @endif
    @hasSection('big-cards')
    @yield('big-cards')
    @endif
    @endif
    <!-- End Home Page Sections -->

    <!-- My Profile Section -->
    @hasSection('myprofile')
    @yield('myprofile')
    @endif
    <!-- End My Profile Section -->

    <!-- Pet Profile Sections -->
    @hasSection('petImage')
    @yield('petImage')
    @endif

    @hasSection('petInfo')
    @yield('petInfo')
    @endif

    @hasSection('askAboutPet')
    @yield('askAboutPet')
    @endif

    @hasSection('guestForm')
    @yield('guestForm')
    @endif

    @if ($page_title === 'Pet Profile')
    @hasSection('recommended-cards')
    @yield('recommended-cards')
    @endif
    @hasSection('big-cards')
    @yield('big-cards')
    @endif
    @endif

    @hasSection('modalForPetImage')
    @yield('modalForPetImage')
    @endif
    <!-- End Pet Profile Sections -->

    @if ($page_title === 'Home-Page' || $page_title === 'Pet Profile' || $page_title === 'Dashboard')
    @include('layouts/footer')
    @include('auth/login')
    @include('auth/register')
    @endif

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous">
    </script>
    <script type="text/javascript" src="{{ asset('js/script.js') }}"></script>

</body>

</html>