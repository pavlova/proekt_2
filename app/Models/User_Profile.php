<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_Profile extends Model
{
    protected $table = 'user_profiles';
    
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function countries()
    {
        return $this->belongsTo('App\Country');
    }

    public function states()
    {
        return $this->belongsTo('App\State');
    }

    public function pet_profiles()
    {
        return $this->hasMany('App\Pet_Profile');
    }
}
