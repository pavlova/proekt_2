@extends ('layouts/master')

@section('search')
<div class="container-fluid searchForm-e">
    <form method="POST" action={{ route('search.post')}}>
        @csrf
        <div class="form-group col-md-2 col-md-offset-1 col-sm-3 text-center">
            <label for="sizeSelect">SIZE</label>
            <select class="form-control" name="sizeSelect">
                <option value="">Any</option>
                @foreach($sizes as $size)
                <option value="{{ $size->id }}">{{ $size->pet_size }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-2 col-sm-3 text-center">
            <label for="coatLenghtSelect">COAT LENGHT</label>
            <select class="form-control" name="coatLenghtSelect">
                <option value="">Any</option>
                @foreach($coat_lenghts as $coat_lenght)
                <option value="{{ $coat_lenght->id }}">{{ $coat_lenght->coat_length }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-2 col-sm-3 text-center">
            <label for="genderSelect">Gender</label>
            <select class="form-control" name="genderSelect">
                <option value="">Any</option>
                @foreach($genders as $gender)
                <option value="{{ $gender->id }}">{{ $gender->gender }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-default btnSearchForm">Search</button>
    </form>
</div>
@stop

@section('cards')
<!-- CARDS -->
<div class="container-fluid container-fluid-CARD">
    <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 text-center">
        @php
        $count = 0;
        @endphp
        @foreach($petInfo as $pet)
        <div @if ($count==0 || $count%5==0)
            class="col-md-2 col-md-offset-1 col-sm-4 col-xs-12 card recommendations-body-inner" @else
            class="col-md-2 col-sm-4 col-xs-12 card recommendations-body-inner" @endif>
            @php
            $count++;
            @endphp
            <div class="flip-card">
                <div class="flip-card-inner">
                    <div class="petCard flip-card-front">
                        <div class="card-img">
                            <img src="{{ URL::to('/images/petImages/'.$pet['image']) }}" alt="" class="imageCARD">
                        </div>
                        <div class="card-body">
                            <h5>{{ $pet['name'] }}</h5>
                        </div>
                    </div>
                    <div class="petCard-hover flip-card-back">
                        <div class="card-img-hover">
                            <img src="{{ URL::to('/images/petImages/'.$pet['image']) }}" class="img-circle">
                            <a class="btn btn-circle" href="{{ route('petprofile', ['id' => $pet['id']]) }}"
                                target="_blank"><i class="fas fa-share"></i></a>
                        </div>
                        <div class="card-body-hover">
                            <h4>{{ $pet['name'] }}</h4>
                            <h5>{{ $pet['type'] }}</h5>
                            <h5>{{ $pet['size'] }} {{ $pet['gender'] }}</h5>
                            <h5>{{ $pet['location'] }}</h5>
                        </div>
                        <div class="card-body-active">
                            <h3 class="pet-name-h3"><a href="{{ route('petprofile', ['id' => $pet['id']]) }}"
                                    target="_blank">{{ $pet['name'] }} </a></h3>
                            <a rel="nofollow" href="http://www.facebook.com/share.php?u=<;url>"
                                onclick="return fbs_click()" target="_blank" class="fb_share_link"><i
                                    class="fab fa-facebook-f"></i></a>
                            <a
                                href="https://twitter.com/home?status=This%20is%20Awesome%20page!!%20http%3A//yourpage.com"><i
                                    class="fab fa-twitter" target="_blank"></i></a>
                            <a href="http://www.pinterest.com/pin/create/button/?url=<page url (encoded)>&media=<picture url (encoded)>&description=<HTML-encoded description>"
                                target="_blank" class="pinterest-anchor pinterest-hidden"><i
                                    class="fab fa-pinterest-p"></i></a>
                            <a href="mailto:abc@xyz.com?&subject=Your Subject&body=your%20mail%20content"><i
                                    class="fas fa-envelope"></i></a>
                            <a role="button" class="copyToClipboard"
                                data-target="{{ route('petprofile', ['id' => $pet['id']]) }}"><i
                                    class="fas fa-link"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@stop