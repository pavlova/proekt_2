<?php

use Faker\Factory;
use App\Models\User_Profile;
use Illuminate\Database\Seeder;

class User_profilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i=0; $i<10; $i++) {
            $user = new User_Profile();

            $user->first_name = $faker->firstName;
            $user->last_name = $faker->lastName;
            $user->phone_number = $faker->phoneNumber;
            $user->street_address = $faker->streetName;
            $user->street_num = $faker->buildingNumber;
            $user->city = $faker->city;
            $user->zip_code = $faker->postcode;
            $user->postal_code = $faker->postcode;
            $user->email = $faker->email;
            $user->password = $faker->password;
            $user->is_guest = 0;
            $user->country_id = rand(1, 246);
            $user->state_id = rand(1, 4121);
            $user->save();

        }
    }
}
