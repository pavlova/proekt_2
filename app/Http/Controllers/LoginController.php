<?php

namespace App\Http\Controllers;

use App\Models\User_Profile;
use Illuminate\Http\Request;
use App\Http\Requests\LoginValidationRequest;

class LoginController extends Controller
{
    public function sendLoginPost(LoginValidationRequest $request)
    {
        $email = $request->email;
        $password = $request->password;
        $login = 0;
        $userArr = User_Profile::where(['email' => $email, 'password' => $password])->get();

        if (count($userArr)>0) {
            $login = 1;
            $user_id = $firstname = $lastname = 
            $postalcode = $email = $password = '';

            foreach ($userArr as $user) {
                $user_id = $user['id'];
                $firstname = $user['first_name'];
                $lastname = $user['last_name'];
                $postalcode = $user['postal_code'];
                $email = $user['email'];
                $password = $user['password'];
            }
            
            if ($login > 0) {
                session()->put(['id' => $user_id, 'firstname' => $firstname, 'lastname' => $lastname, 'postalCode' => $postalcode, 'emailR' => $email, 'passwordR' => $password]);
            }
            return redirect()->route('homepage');

        } else {
            
            return redirect()->route('homepage')->with('error', 'Wrong email or password, please try again!');
        }

    }

    public function logout()
    {
        session()->forget(['id', 'firstname', 'lastname', 'postalCode', 'emailR', 'passwordR']);
        return redirect()->route('homepage');
    }

}