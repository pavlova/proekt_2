<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MyProfileValidationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [          
            "firstMyProfile" => "required|alpha|max:25",
            "lastMyProfile" => "required|alpha|max:25",
            "phoneMyProfile" => "required|max:25",
            "countryMyProfile" => "required",
            "streetMyProfile" => "required|max:64",
            "streetMyProfileC" => "required|max:64",
            "cityMyProfile" => "required|max:25",
            "stateMyProfile" => "required",
            "zipCodeMyProfile" => "required|numeric",
            "petName" => "required|alpha|max:25",
            "petType" => "required",
            "petLocation" => "required|max:64",
            "petShortDesc" => "required|max:200",
            "petCharacteristics" => "required|max:64",
            "petCoatLength" => "required",
            "petHouseTrained" => "required",
            "petGoodInHome" => "required|max:120",
            "petSize" => "required",
            "petGender" => "required",
            "petLongDesc" => "required|max:600",
        ];
    }
}
