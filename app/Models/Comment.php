<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function pet_profiles()
    {
        return $this->belongsTo('App\Pet_Profile');
    }

    public function user_profiles()
    {
        return $this->belongsTo('App\User_Profile');
    }
}
